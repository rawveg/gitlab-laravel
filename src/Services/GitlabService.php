<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Contracts\Serviceable;

class GitlabService extends AbstractServiceClass
{
    public function bootService(): void
    {
        $this->baseUrl = config('gitlab.base_url');
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function commits(): Serviceable|CommitService
    {
        return $this->hasService(CommitService::class);
    }

    public function branches(): Serviceable|BranchService
    {
        return $this->hasService(BranchService::class);
    }

    public function pipelines(): Serviceable|PipelineService
    {
        return $this->hasService(PipelineService::class);
    }

    public function projects(): Serviceable|ProjectService
    {
        return $this->hasService(ProjectService::class);
    }

    public function mergeRequests(): Serviceable|MergeRequestService
    {
        return $this->hasService(MergeRequestService::class);
    }

    public function tags(): Serviceable|TagsService
    {
        return $this->hasService(TagsService::class);
    }

    public function variables(): Serviceable|VariablesService
    {
        return $this->hasService(VariablesService::class);
    }

    public function triggers(): Serviceable|TriggerService
    {
        return $this->hasService(TriggerService::class);
    }

    public function triggerTokens(): Serviceable|TriggerTokenService
    {
        return $this->hasService(TriggerTokenService::class);
    }

    public function groups(): Serviceable|GroupsService
    {
        return $this->hasService(GroupsService::class);
    }
}

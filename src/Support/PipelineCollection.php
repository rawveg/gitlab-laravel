<?php

namespace Rawveg\Gitlab\Support;

use Illuminate\Support\Collection;

class PipelineCollection extends Collection
{
    public function whereLike($attribute, $search): PipelineCollection
    {
        return $this->filter(function ($item) use ($attribute, $search) {
            if (stripos($item[$attribute], $search) !== false) {
                return true;
            }

            return false;
        });
    }
}

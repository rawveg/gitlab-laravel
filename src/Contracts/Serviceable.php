<?php

namespace Rawveg\Gitlab\Contracts;

interface Serviceable
{
    public function select(?array $keys = null): self;

    public function where(string $attribute, string $value): self;
}

<?php

namespace Rawveg\Gitlab\Contracts;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Rawveg\Gitlab\Exceptions\GitlabServiceException;
use Rawveg\Gitlab\Facades\Gitlab;

abstract class AbstractServiceClass implements Serviceable
{
    protected mixed $client = null;

    protected string $baseUrl;

    protected array $headers = [];

    protected array $filters = [];

    protected ?array $keys = null;

    protected ?int $projectId = null;

    protected int $totalPages = 100;

    protected int $perPage = 100;

    protected ?string $cacheKey = null;

    /**
     * The array of booted services.
     *
     * @var array
     */
    protected static array $booted = [];

    /**
     * @throws GitlabServiceException
     */
    public function __construct()
    {
        if (! method_exists($this, 'bootService')) {
            throw new GitlabServiceException('Method bootService not implemented');
        }
        $this->headers = [
            'PRIVATE-TOKEN' => config('gitlab.token'),
        ];
        $this->filters = [
            'per_page' => $this->perPage,
        ];
        $this->bootIfNotBooted();
        $this->bootService();
        $this->getClient();
    }

    public function getClient(): mixed
    {
        if ($this->client) {
            return $this->client;
        }
        $this->client = Http::withHeaders($this->headers())->baseUrl($this->getBaseUrl());

        return $this->client;
    }

    public function __call(string $method, array $parameters): static|array|Collection|Serviceable
    {
        if (Collection::hasMacro(Str::snake(Str::replaceFirst(search: 'where', replace: '', subject: $method)))) {
            return $this;
        }
        if (Str::startsWith(haystack: $method, needles: 'andWhere')) {
            $method = Str::replaceFirst(search: 'andWhere', replace: 'where', subject: $method);
        }
        if (Str::startsWith(haystack: $method, needles: 'where')) {
            $attribute = Str::snake(Str::replaceFirst(search: 'where', replace: '', subject: $method));
            return $this->where(attribute: $attribute, value: isset($parameters[0]) ? $parameters[0] : '');
        }

        if ($method === 'all') {
            return $this->get();
        }

        return $this;
    }

    /**
     * Check if the service needs to be booted and if so, do it.
     *
     * @return void
     */
    protected function bootIfNotBooted(): void
    {
        if (! isset(static::$booted[static::class])) {
            static::$booted[static::class] = true;
            static::boot();
        }
    }

    /**
     * Bootstrap the service and its traits.
     *
     * @return void
     */
    protected static function boot(): void
    {
        static::bootTraits();
    }

    /**
     * Boot all the bootable traits on the service.
     *
     * @return void
     */
    protected static function bootTraits(): void
    {
        $class = static::class;
        $booted = [];

        foreach (class_uses_recursive($class) as $trait) {
            $method = 'boot'.class_basename($trait);

            if (method_exists($class, $method) && ! in_array($method, $booted)) {
                forward_static_call([$class, $method]);

                $booted[] = $method;
            }
        }
    }

    /**
     * @throws ConnectionException
     */
    protected function executeQuery(string $url, array $filters = []): array
    {
        // Generate a unique cache key
        if ($this->getCacheKey() === null) {
            $this->setCacheKey(
                md5(
                    $url . serialize($filters) . serialize($this->filters()) .
                    $this->getTotalPages() . ':' . $this->perPage
                )
            );
        }
        $cacheKey = md5(
            $url.serialize($filters).serialize($this->filters()).
            $this->getTotalPages().':'.$this->perPage
        );
        $cacheTtl = config('gitlab.cache.ttl');

        // Use the remember method to store the results
        return Cache::remember($this->getCacheKey(), $cacheTtl, function () use ($url, $filters) {
            $responses = Gitlab::getClient()->pool(function (Pool $pool) use ($url, $filters) {
                foreach (range(1, $this->getTotalPages()) as $page) {
                    $filters = [...$this->filters(), ...$filters, ...['page' => $page]];
                    $pool->withHeaders($this->headers())
                        ->baseUrl(config('gitlab.base_url'))
                        ->get($url, [...$this->filters(), ...$filters, ...['page' => $page]]);
                }
            });

            $retVal = [];

            foreach ($responses as $response) {
                if ($response?->status() === 200) {
                    $retVal = [...$retVal, ...$response->json()];
                }
            }

            if (! isset($retVal[0])) {
                return [$retVal];
            }

            return $retVal;
        });
    }

    public function hasService(string $class): Serviceable
    {
        $service = app($class);
        $service->setProjectId($this->getProjectId());
        $service->select($this->keys());

        return $service;
    }

    /**
     * @throws GitlabServiceException
     */
    public function get(): array|Collection
    {
        try {
            return (new Collection(
                $this->executeQuery(url: $this->getBaseUrl(), filters: $this->filters())
            ))
                ->select($this->keys());
        } catch (ConnectionException $e) {
            throw new GitlabServiceException('Failed to get Collection', $e->getCode(), $e);
        }
    }

    public function setCacheKey(string $keyName): void
    {
        $this->cacheKey = md5($keyName);
    }

    protected function getCacheKey(): ?string
    {
        return $this->cacheKey;
    }

    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setProjectId(?int $id): self
    {
        $this->projectId = $id;

        return $this;
    }

    public function whereProjectId(int $projectId): self
    {
        $this->setProjectId($projectId);

        return $this;
    }

    public function whereNull(string $attribute): self
    {
        $this->filters[$attribute] = null;

        return $this;
    }

    public function getProjectId(): ?int
    {
        return $this->projectId ?? config('gitlab.defaults.project_id');
    }

    public function setTotalPages(int $totalPages): self
    {
        $this->totalPages = $totalPages;

        return $this;
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    public function select(?array $keys = null): self
    {
        $this->keys = $keys;

        return $this;
    }

    public function where(string $attribute, string $value): self
    {
        $this->filters[$attribute] = $value;

        return $this;
    }

    public function headers(): array
    {
        return $this->headers;
    }

    public function filters(): array
    {
        return $this->filters;
    }

    public function keys(): ?array
    {
        return $this->keys;
    }

    public function first(): object
    {
        return (object) $this->get()->first();
    }

    public function last(bool $toJson = false): object
    {
        return (object) $this->get()->last();
    }

    public function count(): int
    {
        return $this->get()->count();
    }
}

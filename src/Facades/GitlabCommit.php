<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\CommitService;

/**
 * @method array first() Get the first record matching the attributes or return null
 * @method string getSha() Get the sha of the commit
 * @method CommitService select(string[] $select) Select the fields to retrieve
 * @method CommitService setSha(string $sha) Set the sha of the commit
 * @method CommitService whereId(?string $id) Filter the commits by their id
 * @method CommitService whereProjectId(int $projectId) Filter the commits by their project id
 */
class GitlabCommit extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return CommitService::class;
    }
}

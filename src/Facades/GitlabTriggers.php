<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\TriggerService;

class GitlabTriggers extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return TriggerService::class;
    }
}

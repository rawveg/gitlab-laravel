<?php

namespace Rawveg\Gitlab\Concerns;

use DirectoryIterator;
use ReflectionClass;

trait LoadMacros
{
    /**
     * Load the macros.
     */
    public static function bootLoadMacros(): void
    {
        $baseNamespace = 'Rawveg\\Gitlab\\Macros\\';
        $directoryPath = __DIR__ . '/../Macros/';

        $iterator = new DirectoryIterator($directoryPath);

        foreach ($iterator as $file) {
            if ($file->isFile() && $file->getExtension() === 'php') {
                $className = $baseNamespace . $file->getBasename('.php');

                if (class_exists($className)) {
                    $reflection = new ReflectionClass($className);
                    if (
                        $reflection->implementsInterface('Rawveg\\Gitlab\\Contracts\\Macroable') &&
                        $reflection->hasMethod('boot')
                    ) {
                        $className::boot();
                    }
                }
            }
        }
    }
}

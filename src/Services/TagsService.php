<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Concerns\LoadMacros;
use Rawveg\Gitlab\Contracts\AbstractServiceClass;

class TagsService extends AbstractServiceClass
{
    use LoadMacros;

    protected string $baseUrl = '/api/v4/projects/%s/repository/tags';

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId());
    }
}

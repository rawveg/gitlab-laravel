<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\BranchService;

class GitlabBranch extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return BranchService::class;
    }
}

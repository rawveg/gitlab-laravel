FROM php:8.2-apache
MAINTAINER Tim Green <rawveg@gmail.com>

RUN apt-get update && apt-get install -yqq libzip-dev unzip libonig-dev libxml2-dev wget
RUN docker-php-ext-install pdo_mysql zip mbstring pcntl bcmath
RUN pecl install pcov && docker-php-ext-enable pcov
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

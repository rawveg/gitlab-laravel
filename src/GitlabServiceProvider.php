<?php

namespace Rawveg\Gitlab;

use Carbon\Carbon;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;
use Rawveg\Gitlab\Services\BranchService;
use Rawveg\Gitlab\Services\CommitService;
use Rawveg\Gitlab\Services\GitlabService;
use Rawveg\Gitlab\Services\GroupsService;
use Rawveg\Gitlab\Services\MergeRequestService;
use Rawveg\Gitlab\Services\PipelineService;
use Rawveg\Gitlab\Services\ProjectService;
use Rawveg\Gitlab\Services\TagsService;
use Rawveg\Gitlab\Services\TriggerService;
use Rawveg\Gitlab\Services\TriggerTokenService;
use Rawveg\Gitlab\Services\VariablesService;

class GitlabServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function boot(): void
    {
        $this->setupConfig();
    }

    public function setUpConfig(): void
    {
        $source = realpath($raw = __DIR__.'/../config/gitlab.php') ?: $raw;
        $configEnv = realpath($configRaw = __DIR__.'/Templates/Configuration.env') ?: $configRaw;
        $this->publishes([$source => config_path('gitlab.php')], 'config');

        // Append variables to .env.example file
        $envExamplePath = base_path('.env.example');

        if (file_exists($envExamplePath)) {
            $envExample = file_get_contents($envExamplePath);
            if (!str_contains($envExample, 'GITLAB_BASE_URL')) {
                $envExample .= file_get_contents($configEnv);
                file_put_contents($envExamplePath, $envExample);
            }
        }
    }

    /**
     * Register services.
     */
    public function register(): void
    {
        collect($this->provides())
            ->each(fn ($service) => $this->app->bind($service, fn () => new $service()));

        Http::macro('gitlab', function () {
            return Http::withHeaders([
                'PRIVATE-TOKEN' => config('gitlab.token'),
            ])->baseUrl(config('gitlab.base_url'));
        });

        Collection::macro('whereTimeBetween', function ($column, $start, $end) {
            return $this->filter(function ($value) use ($column, $start, $end) {
                return Carbon::parse($value->{$column})->between($start, $end);
            });
        });
    }

    public function provides(): array
    {
        return [
            GitlabService::class,
            ProjectService::class,
            PipelineService::class,
            MergeRequestService::class,
            BranchService::class,
            CommitService::class,
            TagsService::class,
            VariablesService::class,
            GroupsService::class,
            TriggerService::class,
            TriggerTokenService::class,
        ];
    }
}

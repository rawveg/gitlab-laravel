<?php

namespace Rawveg\Gitlab\Services;

use Illuminate\Support\Collection;
use Rawveg\Gitlab\Concerns\LoadMacros;
use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Exceptions\GitlabServiceException;
use Rawveg\Gitlab\Facades\Gitlab;
use Rawveg\Gitlab\Helpers\SubCollection;

class BranchService extends AbstractServiceClass
{
    use LoadMacros;

    protected string $baseUrl = '/api/v4/projects/%s/repository/branches';

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId());
    }

    public function commits(): SubCollection
    {
        $key = 'commit';
        $collection = $this->select(['commit'])->get();

        return new SubCollection($collection, $key);
    }

    /**
     * @throws GitlabServiceException
     */
    public function getActive(): Collection
    {
        return $this->getByState('active');
    }

    /**
     * @throws GitlabServiceException
     */
    public function getClosed(): Collection
    {
        return $this->getByState('closed');
    }

    /**
     * @throws GitlabServiceException
     */
    public function getMerged(): Collection
    {
        return $this->getByState('merged');
    }

    /**
     * @throws GitlabServiceException
     */
    private function getByState(string $state = 'active'): Collection
    {
        $gitlab = Gitlab::setProjectId($this->getProjectId());
        return $gitlab->branches()
            ->select(["name"])
            ->get()
            ->filter(function ($branch) use ($gitlab, $state) {
                if (in_array($branch['name'], ['master', 'main']) && $state === 'active') {
                    return true;
                }
                return !empty($gitlab->mergeRequests()
                    ->whereSourceBranch($branch['name'])
                    ->whereState($state)
                    ->get()
                    ->flatten()
                    ->toArray()
                );
            })
            ->flatten();
    }
}

<?php

namespace Rawveg\Gitlab\Helpers;

use Illuminate\Support\Collection;

class SubCollection
{
    public function __construct(
        private Collection $collection,
        private string $key
    ) {
        //
    }

    public function get(): Collection
    {
        return collect($this->collection->map(function ($item) {
            return $item[$this->key];
        }));
    }
}

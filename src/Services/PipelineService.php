<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Concerns\LoadMacros;
use Rawveg\Gitlab\Contracts\AbstractServiceClass;

/**
 * @method PipelineService whereIid(int $pipelineIid) Filter pipelines by their internal id
 * @method PipelineService whereStatus(string $status) Filter pipelines by status
 * @method PipelineService whereRef(string $ref) Filter pipelines by ref
 * @method PipelineService whereSha(string $sha) Filter pipelines by sha
 * @method PipelineService select(string[] $attributes) Select specific attributes
 * @method PipelineService whereProjectId(int|null $projectId) Filter pipelines by project id
 * @method object first() Get the first pipeline
 * @method object last() Get the last pipeline
 * @method int count() Count the number of pipelines
 */
class PipelineService extends AbstractServiceClass
{
    use LoadMacros;

    protected string $baseUrl = '/api/v4/projects/%s/pipelines/%s';
    protected ?int $pipelineId = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function wherePipelineId(?int $id): self
    {
        $this->setPipelineId($id);
        return $this;
    }

    public function getPipelineId(): ?int
    {
        return $this->pipelineId;
    }

    public function setPipelineId(int $pipelineId): self
    {
        $this->pipelineId = $pipelineId;
        return $this;
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId(), $this->getPipelineId());
    }
}

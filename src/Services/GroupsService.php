<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;

class GroupsService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/groups/%s';
    protected ?int $groupId = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.group_id') !== null) {
            $this->setGroupId(config('gitlab.defaults.group_id'));
        }
    }

    public function setGroupId(int $groupId): self
    {
        $this->groupId = $groupId;
        return $this;
    }

    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * @throws \Exception
     */
    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getGroupId());
    }

    public function projects(): self
    {
        $this->baseUrl .= '/projects';
        return $this;
    }
}

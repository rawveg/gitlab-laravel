<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\TagsService;

class GitlabTags extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return TagsService::class;
    }
}

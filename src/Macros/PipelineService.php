<?php

namespace Rawveg\Gitlab\Macros;

use Illuminate\Support\Collection;
use Rawveg\Gitlab\Contracts\AbstractMacro;
use Rawveg\Gitlab\Contracts\Macroable;

class PipelineService extends AbstractMacro implements Macroable
{
    public static array $attributes = [
        'id',
        'iid',
        'project_id',
        'sha',
        'ref',
        'status',
        'source',
        'created_at',
        'updated_at',
        'web_url',
        'name',
    ];

    public static array $timeAttributes = [
        'created_at',
        'updated_at',
    ];

    /**
     * Boot the macro.
     * @throws \Exception
     */
    public static function boot(): void
    {
        collect(['where', 'whereBetween', 'whereBoolean'])
            ->each(fn ($prefix) => static::processMacro($prefix));
    }
}

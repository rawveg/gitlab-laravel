<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;

class VariablesService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/projects/%s/variables';

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId());
    }
}

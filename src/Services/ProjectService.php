<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Contracts\Serviceable;

class ProjectService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/projects/%s';

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId());
    }

    public function commits(): Serviceable|CommitService
    {
        return $this->hasService(CommitService::class);
    }

    public function branches(): Serviceable|BranchService
    {
        return $this->hasService(BranchService::class);
    }

    public function pipelines(): Serviceable|PipelineService
    {
        return $this->hasService(PipelineService::class);
    }

    public function mergeRequests(): Serviceable|MergeRequestService
    {
        return $this->hasService(MergeRequestService::class);
    }
}

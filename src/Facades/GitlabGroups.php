<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\GroupsService;

/**
 * @method static setGroupId(int $int)
 */
class GitlabGroups extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return GroupsService::class;
    }
}

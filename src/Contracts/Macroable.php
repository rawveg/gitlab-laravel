<?php

namespace Rawveg\Gitlab\Contracts;

interface Macroable
{
    /**
     * Boot the macro.
     */
    public static function boot(): void;
}

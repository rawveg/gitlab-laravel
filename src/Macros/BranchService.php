<?php

namespace Rawveg\Gitlab\Macros;

use Illuminate\Support\Collection;
use Rawveg\Gitlab\Contracts\AbstractMacro;
use Rawveg\Gitlab\Contracts\Macroable;

class BranchService extends AbstractMacro implements Macroable
{
    public static array $attributes = [
        'name',
        'merged',
        'protected',
        'developers_can_push',
        'developers_can_merge',
        'can_push',
        'default',
        'web_url',
    ];

    /**
     * Boot the macro.
     * @throws \Exception
     */
    public static function boot(): void
    {
        collect(['where', 'whereBoolean'])
            ->each(fn ($prefix) => static::processMacro($prefix));
    }
}

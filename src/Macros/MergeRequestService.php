<?php

namespace Rawveg\Gitlab\Macros;

use Illuminate\Support\Collection;
use Rawveg\Gitlab\Contracts\AbstractMacro;
use Rawveg\Gitlab\Contracts\Macroable;

class MergeRequestService extends AbstractMacro implements Macroable
{
    public static array $attributes = [
        'id',
        'iid',
        'project_id',
        'title',
        'description',
        'state',
    ];

    public static array $timeAttributes = [
        'created_at',
        'updated_at',
    ];

    /**
     * Boot the macro.
     * @throws \Exception
     */
    public static function boot(): void
    {
        collect(['where', 'whereBoolean', 'whereForState'])
            ->each(fn ($prefix) => static::processMacro($prefix));
    }
}

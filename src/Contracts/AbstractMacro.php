<?php

namespace Rawveg\Gitlab\Contracts;

use Carbon\Carbon;
use Illuminate\Support\Collection;

abstract class AbstractMacro implements Macroable
{
    public static array $attributes = [];

    /**
     * @throws \Exception
     */
    public static function processMacro(string $prefix): void
    {
        match ($prefix) {
            'where' => static::processWhere($prefix),
            'whereBetween' => static::processWhereBetween(),
            'whereLike' => static::processWhereLike(),
            'whereBoolean' => static::processWhereBoolean(),
            'whereForState' => static::processWhereForState(),
            default => throw new \Exception('Invalid prefix'),
        };
    }

    private static function processWhere(string $prefix): void
    {
        collect(static::$attributes)
            ->each(function ($attribute) use ($prefix) {
                Collection::macro(
                    static::getCommand($prefix, $attribute),
                    function (string|int $value) use ($attribute) {
                        return $this->filter($this->operatorForWhere(key: $attribute, operator: '=', value: $value));
                    }
                );
            });
    }

    private static function processWhereBetween(): void
    {
        collect(static::$timeAttributes)
            ->each(function ($attribute) {
                Collection::macro(
                    static::getCommand('where', $attribute, 'Between'),
                    function (string|int $start, string|int $end) use ($attribute) {
                        return $this->where($attribute, '>=', $start)->where($attribute, '<=', $end);
                    }
                );
                Collection::macro(
                    static::getCommand('where',  $attribute, 'NotBetween'),
                    function (string|int $start, string|int $end) use ($attribute) {
                        return $this->where($attribute, '<', $start)->where($attribute, '>', $end);
                    }
                );
                Collection::macro(
                    static::getCommand('where',  $attribute, 'After'),
                    function (string|int $value) use ($attribute) {
                        return $this->where($attribute, '>', $value);
                    }
                );
                Collection::macro(
                    static::getCommand('where',  $attribute, 'Before'),
                    function (string|int $value) use ($attribute) {
                        return $this->where($attribute, '<', $value);
                    }
                );
            });
    }

    private static function processWhereLike(): void
    {
        collect(static::$attributes)
            ->each(function ($attribute) {
                Collection::macro(
                    static::getCommand('where', $attribute, 'Like'),
                    function (string $value) use ($attribute) {
                        return $this->filter(function ($item) use ($value, $attribute) {
                            return false !== stripos($item[$attribute], $value);
                        });
                    }
                );
                Collection::macro(
                    static::getCommand('where', $attribute, 'NotLike'),
                    function (string $value) use ($attribute) {
                        return $this->filter(function ($item) use ($value, $attribute) {
                            return false === stripos($item[$attribute], $value);
                        });
                    }
                );
                Collection::macro(
                    static::getCommand('where', $attribute, 'IsEnvironment'),
                    function (string $value) use ($attribute) {
                        return match ($value) {
                            'production' => $this->filter(function ($item) use ($attribute) {
                                return false === stripos($item[$attribute], 'cat');
                            })->filter(function ($item) use ($attribute) {
                                return false === stripos($item[$attribute], 'test');
                            }),
                            'test' => $this->filter(function ($item) use ($attribute) {
                                return false !== stripos($item[$attribute], 'test');
                            }),
                            'cat' => $this->filter(function ($item) use ($attribute) {
                                return false !== stripos($item[$attribute], 'cat');
                            }),
                            default => false,
                        };
                    }
                );
            });
    }

    private static function processWhereBoolean(): void
    {
        collect(static::$attributes)
            ->each(function ($attribute) {
                Collection::macro(
                    static::getCommand('where', $attribute, 'IsFalse'),
                    function () use ($attribute) {
                        return $this->where($attribute, false);
                    }
                );
                Collection::macro(
                    static::getCommand('where', $attribute, 'IsTrue'),
                    function () use ($attribute) {
                        return $this->where($attribute, true);
                    }
                );
            });
    }

    private static function processWhereForState(): void
    {
        $attribute = 'state';
        Collection::macro(
            static::getCommand('where', $attribute, 'IsActive'),
            function () use ($attribute) {
                return $this->where($attribute, 'active');
            }
        );
        Collection::macro(
            static::getCommand('where', $attribute, 'IsNotActive'),
            function () use ($attribute) {
                return $this->where($attribute, '!=', 'active');
            }
        );
    }

    public static function getCommand(string $prefix, string $attribute, ?string $suffix = ''): string
    {
        return $prefix . str_replace('_', '', ucwords($attribute, '_')) . $suffix;
    }
}

<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\TriggerTokenService;

/**
 * @method static setProjectId(int|null $getProjectId)
 * @method static setTriggerId($id)
 */
class GitlabTriggerTokens extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return TriggerTokenService::class;
    }
}

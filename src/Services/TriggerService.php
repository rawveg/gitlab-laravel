<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Facades\Gitlab;
use Rawveg\Gitlab\Facades\GitlabTriggerTokens;

class TriggerService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/projects/%s/trigger/%s';
    protected ?string $triggerType = null;
    protected ?string $triggerRef = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function setTriggerType(?string $triggerType): self
    {
        $this->triggerType = $triggerType;
        return $this;
    }

    public function getTriggerType(): ?string
    {
        return $this->triggerType;
    }

    public function setTriggerRef(?string $triggerRef): self
    {
        $this->triggerRef = $triggerRef;
        return $this;
    }

    public function getTriggerRef(): ?string
    {
        return $this->triggerRef;
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId(), $this->getTriggerType());
    }

    public function pipeline(): self
    {
        $this->triggerType = 'pipeline';
        return $this;
    }

    public function branch(string $branch): self
    {
        $this->triggerRef = $branch;
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function run(): object
    {
        if ($this->getTriggerType() === null) {
            throw new \Exception('Trigger type is required to trigger a pipeline');
        }
        $trigger = Gitlab::setProjectId($this->getProjectId())
            ->triggerTokens()
            ->create('Trigger for ' . $this->getTriggerType() . ' ' . $this->getTriggerRef());

        $result = Gitlab::getClient()
            ->withQueryParameters([
                'token' => $trigger->token,
                'ref' => $this->getTriggerRef(),
            ])
            ->post($this->getBaseUrl())
            ->throw()
            ->object();

        GitlabTriggerTokens::setTriggerId($trigger->id)->delete();

        return $result;
    }
}

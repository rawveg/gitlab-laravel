<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Http;
use Rawveg\Gitlab\Contracts\Serviceable;
use Rawveg\Gitlab\Services\BranchService;
use Rawveg\Gitlab\Services\GitlabService;
use Rawveg\Gitlab\Services\GroupsService;
use Rawveg\Gitlab\Services\MergeRequestService;
use Rawveg\Gitlab\Services\TriggerService;
use Rawveg\Gitlab\Services\TriggerTokenService;

/**
 * @method Collection pipelines() Loads the Pipelines Service without going through its Facade
 * @method Collection addToQuery(string $string, string $string1) Add a query string to the URL
 * @method object get(string $url, array $filters = [], int $totalPages = 100) Get the data from the Gitlab API
 * @method Http getClient() Returns the Http Facade Client
 * @method Serviceable|TriggerTokenService triggerTokens() Loads the Trigger Tokens Service without going through its Facade
 * @method self setProjectId(int|null $getProjectId) Set the Project ID
 * @method BranchService branches()
 * @method MergeRequestService mergeRequests()
 * @method TriggerService triggers()
 * @method GroupsService groups()
 */
class Gitlab extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return GitlabService::class;
    }
}

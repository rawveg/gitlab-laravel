<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\MergeRequestService;

class GitlabMergeRequest extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return MergeRequestService::class;
    }
}

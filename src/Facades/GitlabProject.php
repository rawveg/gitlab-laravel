<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\ProjectService;

class GitlabProject extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return ProjectService::class;
    }
}

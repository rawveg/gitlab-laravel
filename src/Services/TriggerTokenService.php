<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Facades\Gitlab;

class TriggerTokenService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/projects/%s/triggers/%s';
    protected ?int $triggerId = null;
    protected ?string $token = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    public function setTriggerId(?int $triggerId): self
    {
        $this->triggerId = $triggerId;
        return $this;
    }

    public function getTriggerId(): ?int
    {
        return $this->triggerId;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId(), $this->getTriggerId());
    }

    /**
     * Create a new trigger token
     *
     * @param string $description
     * @return object
     */
    public function create(string $description): object
    {
        $this->setTriggerId(null);

        $response = (object) Gitlab::getClient()->post($this->getBaseUrl(), ['description' => $description])->json();
        $this->setTriggerId($response->id);
        return $response;
    }

    public function show(): object
    {
        if($this->getTriggerId() === null) {
            throw new \Exception('Trigger id is required to show a trigger token');
        }
        return (object) Gitlab::getClient()->get($this->getBaseUrl())->json();
    }

    public function update(string $description): object
    {
        if($this->getTriggerId() === null) {
            throw new \Exception('Trigger id is required to update a trigger token');
        }
        return (object) Gitlab::getClient()->patch($this->getBaseUrl(), ['description' => $description])->json();
    }

    /**
     * @throws \Exception
     */
    public function delete(): void
    {
        if($this->getTriggerId() === null) {
            throw new \Exception('Trigger id is required to delete a trigger token');
        }
        Gitlab::getClient()->delete($this->getBaseUrl());
    }
}

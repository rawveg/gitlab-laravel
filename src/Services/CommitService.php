<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Contracts\AbstractServiceClass;
use Rawveg\Gitlab\Contracts\Serviceable;

/**
 * @method CommitService select(string[] $select) Select the fields to retrieve
 * @method CommitService whereSha(string $sha) Filter the commits by their sha
 * @method CommitService whereProjectId(int $projectId) Filter the commits by their project id
 */
class CommitService extends AbstractServiceClass
{
    protected string $baseUrl = '/api/v4/projects/%s/repository/commits/%s';
    protected ?string $sha = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
    }

    /**
     * Filter the commits by their id
     *
     * @param string|null $id
     * @return $this
     */
    public function whereId(?string $id): self
    {
        $this->setSha($id);
        return $this;
    }

    /**
     * Get the sha of the commit
     *
     * @return string|null
     */
    public function getSha(): ?string
    {
        return $this->sha;
    }

    /**
     * Filter the commits by Sha
     *
     * @param string $sha
     * @return $this
     */
    public function setSha(string $sha): self
    {
        $this->sha = $sha;
        return $this;
    }

    public function first(): object
    {
        $first = parent::first();
        $this->setSha($first->sha);
        return $first;
    }

    public function last(bool $toJson = false): object
    {
        $last = parent::last($toJson);
        $this->setSha($last->sha);
        return $last;
    }

    /**
     * Get the base url for the commit service
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId(), $this->getSha(), $this->getSuffix());
    }

    /**
     * Get the pipelines associated with the commit
     *
     * @return Serviceable|PipelineService
     */
    public function pipelines(): Serviceable|PipelineService
    {
        return $this->hasService(PipelineService::class)->whereSha($this->getSha());
    }
}

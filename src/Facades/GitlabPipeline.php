<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\PipelineService;

/**
 * @method PipelineService select(string[] $attributes) Select the columns to be returned.
 * @method PipelineService whereProjectId(int|null $projectId) Filter pipelines by project id.
 * @method PipelineService wherePipelineId(int $pipelineId) Filter pipelines by their internal id.
 * @method PipelineService whereSha(string $sha) Filter pipelines by sha
 * @method Collection all() Get all pipelines.
 */
class GitlabPipeline extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return PipelineService::class;
    }
}

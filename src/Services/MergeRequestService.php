<?php

namespace Rawveg\Gitlab\Services;

use Rawveg\Gitlab\Concerns\LoadMacros;
use Rawveg\Gitlab\Contracts\AbstractServiceClass;

/**
 * @method self whereSourceBranch(mixed $name)
 * @method self whereState(string $state)
 */
class MergeRequestService extends AbstractServiceClass
{
    use LoadMacros;

    protected string $baseUrl = '/api/v4/projects/%s/merge_requests/%s%s';
    protected ?int $iid = null;
    protected ?string $suffix = null;

    public function bootService(): void
    {
        if (config('gitlab.defaults.project_id') !== null) {
            $this->setProjectId(config('gitlab.defaults.project_id'));
        }
        $this->setTotalPages(10);
    }

    public function setIid(?int $iid): self
    {
        $this->iid = $iid;
        return $this;
    }

    public function getIid(): ?int
    {
        return $this->iid;
    }

    public function setSuffix(?string $suffix): self
    {
        $this->suffix = $suffix;
        return $this;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function getBaseUrl(): string
    {
        return sprintf($this->baseUrl, $this->getProjectId(), $this->getIid(), $this->getSuffix());
    }

    public function commits(): self
    {
        $this->setSuffix('/commits');

        return $this;
    }
}

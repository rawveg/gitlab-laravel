<?php

namespace Rawveg\Gitlab\Facades;

use Illuminate\Support\Facades\Facade;
use Rawveg\Gitlab\Services\VariablesService;

/**
 * @method static setProjectId($projectId)
 */
class GitlabVariables extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return VariablesService::class;
    }
}

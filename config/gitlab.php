<?php

declare(strict_types=1);

return [
    'base_url' => env('GITLAB_BASE_URL', 'https://gitlab.com'),
    'token' => env('GITLAB_TOKEN'),
    'defaults' => [
        'group_id' => env('GITLAB_GROUP_ID', null),
        'project_id' => env('GITLAB_PROJECT_ID', null),
    ],
    'cache' => [
        'ttl' => env('GITLAB_CACHE_TTL', 60),
    ],
];

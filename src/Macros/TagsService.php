<?php

namespace Rawveg\Gitlab\Macros;

use Rawveg\Gitlab\Contracts\AbstractMacro;
use Rawveg\Gitlab\Contracts\Macroable;

class TagsService extends AbstractMacro implements Macroable
{
    public static array $attributes = [
        'name',
    ];

    /**
     * Boot the macro.
     * @throws \Exception
     */
    public static function boot(): void
    {
        collect(['where', 'whereLike', 'whereBoolean'])
            ->each(fn ($prefix) => static::processMacro($prefix));
    }
}
